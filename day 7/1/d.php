<?php

function hs($obj){
    if(!is_object($obj)){

        return false;

    }

    return $obj->std;
}
echo "<pre";
$obj=new stdClass();
$obj->std=array('a','b','c');
var_dump(hs(null));
var_dump(hs($obj));
echo "<br>";
?>

<?php

function ts($obj){
    if(!is_object($obj)){

        return false;

    }

    return $obj->std;
}
echo "<pre";
$obj=new stdClass();
$obj->std=array('t','fb','c');
var_dump(ts(null));
var_dump(ts($obj));
?>
